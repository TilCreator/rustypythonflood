#!/usr/bin/env python3
import numpy as np
import PIL
import io
from PIL import Image
from random import shuffle


def img_to_cmds(img_data, weight_bitmap=None, weight_multiplier=2, offset=(0, 0),
                ignore_slighly_transparent_pixels=True, monochrome_pixel_support=False):
    img = Image.open(io.BytesIO(img_data))
    img = img.convert('RGBA')
    arr = np.array(img)

    if weight_bitmap:
        img_weight = Image.open(io.BytesIO(weight_bitmap))
        img_weight = img_weight.convert('RGBA')
        arr_weight = np.array(img_weight)

    out = []
    for i in range(img.size[0]):
        for j in range(img.size[1]):
            # If alpha is bigger than 0 / ignore fully transparent pixels
            if arr[j][i][3] > 0 and (not ignore_slighly_transparent_pixels or arr[j][i][3] == 255):
                if arr[j][i][3] == 255:  # If alpha is 255, don't send it
                    # It pixel in monochrome, use the monochorme encoding
                    if arr[j][i][0] == arr[j][i][1] and arr[j][i][0] == arr[j][i][2] and monochrome_pixel_support:
                        instruction = 'PX %d %d %02x\n' % (i + offset[0], j + offset[1], arr[j][i][0])
                    else:  # Else use the rgb encoding
                        instruction = 'PX %d %d %02x%02x%02x\n' \
                        % (i + offset[0], j + offset[1], arr[j][i][0], arr[j][i][1], arr[j][i][2])
                else:  # Else use the rgbs encoding
                    instruction = 'PX %d %d %02x%02x%02x%02x\n' \
                    % (i + offset[0], j + offset[1], arr[j][i][0], arr[j][i][1], arr[j][i][2], arr[j][i][3])
                if weight_bitmap:
                    print(i, j, arr[j][i][3], arr_weight[j][i][3])
                    for _ in range(round(1 + arr_weight[j][i][3] / 255 * weight_multiplier)):
                        out.append(instruction)
                else:
                    out.append(instruction)

    return out


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Generates a Pixelflood file from an image.',
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('img', help='Input image path')
    parser.add_argument('out', help='Output path')
    parser.add_argument('--weight-img', '-w', nargs='?', default=None,
                        help='Input image for the instuction multiplyer (only the alpha channel is used)')
    parser.add_argument('--weight-multiplier', '-m', nargs='?', default="5", type=float,
                        help='How many times more often a fully weighted pixel would be drawn')
    parser.add_argument('--offset', '-o', nargs='?', default="0:0", help='Offset, default 0:0')
    parser.add_argument('--ignore-slighly-transparent-pixels', '-i', action="store_true",
                        help='Ignores pixels that are slightly transparent')
    parser.add_argument('--monochrome-pixel-support', '-M', action="store_true",
                        help='Set this to true if your pixelflood host supports monochrome pixels (PX <x> <y> <bb>)')

    args = parser.parse_args()
    args.offset = (int(args.offset.split(':')[0]), int(args.offset.split(':')[1]))

    with open(args.out, 'w') as f:
        f.write(''.join(img_to_cmds(img_data=open(args.img, 'rb').read(),
                                    weight_bitmap=open(args.weight_img, 'rb').read() if args.weight_img else None,
                                    weight_multiplier=args.weight_multiplier,
                                    offset=args.offset,
                                    ignore_slighly_transparent_pixels=args.ignore_slighly_transparent_pixels,
                                    monochrome_pixel_support=args.monochrome_pixel_support)))
