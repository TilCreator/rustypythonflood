# <Insert name>
This software is made for streaming an image onto a Pixelflood server.
It has two parts, the Generator which generates Pixelflood instuctions for an image and the tcp dumper which is a high performace tcp streamer with multi thread and multi address (for using multiple interfaces) support.
## Install (local / no root required) (not needed on a server)
This can be skipped on NixOS, just use `nix-shell`
- You need `rust` (+ `cargo`) and `python` (+ `pip`), install those with your package manager of choice
- I suggest also getting `python-venv` and creating a venv to install the python dependencies into
  ```
  $ python -m venv venv
  $ source venv/bin/activate
  ```
- After that (or after skipping the step), you can install the python dependecies
  ```
  $ pip install -r requirements.txt
  ```
- To build the `rustytcpdumper`, do (you will find the finished build in `targtet/release/`)
  ```
  $ cargo build --release
  ```
## Usage
- First generate the instructions from an image (make shure to activate the venv, if you are using it)
  ```
  $ ./gen.py <input_image> <output_file>
  ```
    - See the help for more options
      ```
      $ ./gen.py -h
      ```
- After getting the instuctions file, move it and the `rustytcpdumper` binary onto any server and start it
  ```
  $ rustytcpdumper <instructions_file> <target_host>
  ```
    - Make shure to have a look at the help section, as the dumper is very inefficient in the default state
      ```
      $ rustytcpdumper -h
      ```
