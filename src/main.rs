use clap::{App, Arg};
use rand::{seq::SliceRandom, thread_rng};
use socket2::{Domain, Protocol, Socket, Type};
use std::{
    collections::HashMap,
    env::var,
    fs::File,
    io::{Read, Write},
    net::{IpAddr, SocketAddr, ToSocketAddrs},
    thread,
    time::Duration,
};
use threadpool::ThreadPool;
#[macro_use]
extern crate log;

fn main() {
    let args = App::new(env!("CARGO_PKG_NAME"))
        .version(env!("CARGO_PKG_VERSION"))
        .about("TCP dumper which is able to use multiple ip addresses and threads.\nUse the RUST_LOG env var to set the log level.")
        .arg(
            Arg::with_name("addresses")
                .short("a")
                .long("addresses")
                .takes_value(true)
                .multiple(true)
                .default_value("::")
                .help("List of addresses used, used to bind the socket to the address"),
        )
        .arg(
            Arg::with_name("port")
                .short("p")
                .long("port")
                .takes_value(true)
                .multiple(false)
                .default_value("1234")
                .help("Port of the target host"),
        )
        .arg(
            Arg::with_name("input_file")
                .required(true)
                .help("File to stream"),
        )
        .arg(
            Arg::with_name("target_host")
                .required(true)
                .multiple(true)
                .help("Host to flood, if multiple hosts are presesnt the later ones will be used as fallback"),
        )
        .arg(
            Arg::with_name("thread_count")
                .short("t")
                .long("thread_count")
                .takes_value(true)
                .multiple(false)
                .default_value("1")
                .help("How many threads to spawn per used address"),
        )
        .arg(
            Arg::with_name("offset")
                .short("o")
                .long("offset")
                .takes_value(true)
                .multiple(false)
                .default_value("0:0")
                .help("Offset which will be set with the OFFSET instruction, use the format x:y"),
        )
        .get_matches();

    // Intiate logger with default level on "info"
    pretty_env_logger::formatted_timed_builder()
        .parse_filters(&match var("RUST_LOG") {
            Ok(val) => val,
            Err(_) => String::from("info"),
        })
        .init();

    debug!("{:#?}", args);

    // Parse input args
    let thread_count: usize = args
        .value_of("thread_count")
        .unwrap()
        .parse()
        .expect("Thread count is invalid (has to be an integer)");
    let addresses: Vec<IpAddr> = args
        .values_of("addresses")
        .unwrap()
        .map(|address| {
            address
                .parse::<IpAddr>()
                .expect("Invalid address (ip address expected)")
        })
        .collect();
    let input_file = args.value_of("input_file").unwrap();
    let offset = {
        let offset_format_error = "Offset is invalid (has to be 2 integers divided by a \":\")";
        let mut offset_split = args
            .value_of("offset")
            .unwrap()
            .split(":")
            .map(|coordinate: &str| -> usize { coordinate.parse().expect(offset_format_error) });
        (
            offset_split.next().expect(offset_format_error),
            offset_split.next().expect(offset_format_error),
        )
    };
    let target_address: Vec<SocketAddr> = {
        let target_host: Vec<&str> = args.values_of("target_host").unwrap().collect();
        let port: u16 = args
            .value_of("port")
            .unwrap()
            .parse()
            .expect("Port is invalid (has to be an integer (and u16))");

        target_host
            .iter()
            .map(|host| {
                (*host, port)
                    .to_socket_addrs() // This also resolves any hostname/domain
                    .unwrap()
                    .collect::<Vec<SocketAddr>>()
            })
            .flatten()
            .collect()
    };

    info!("Getting ready to flood {target_address:?} with {input_file} offset to {offset:?} using {thread_count} threads on {addresses_count} addresses",
        target_address=target_address, input_file=input_file, offset=offset,
        thread_count=thread_count * addresses.len(), addresses_count=addresses.len());

    // Load file completly into buffer
    let pixelflood_instructions = {
        let mut f = File::open(args.value_of("input_file").unwrap()).unwrap();
        let mut pixelflood_instructions = Vec::new();
        f.read_to_end(&mut pixelflood_instructions).unwrap();
        pixelflood_instructions
    };

    // Create a HashMap for handeling a threadpool per used address and populate it
    let threadpools: HashMap<IpAddr, ThreadPool> = {
        let mut threadpools = HashMap::new();

        addresses.iter().for_each(|address| {
            threadpools.insert(
                *address,
                ThreadPool::with_name(
                    format!("{} {:?}", env!("CARGO_PKG_NAME"), address),
                    thread_count,
                ),
            );
        });

        threadpools
    };

    debug!("Init done, starting threads...");

    // Main loop
    loop {
        threadpools.iter().for_each(|threadpool| {
            // If the threadpool is not over full by one, add a new job
            // So if a thread crashes it will imedeatly spawn a new one
            if threadpool.1.queued_count() < 1 {
                let address = threadpool.0.clone();
                let target_address = target_address.clone();
                let offset = offset.clone();
                let pixelflood_instructions = pixelflood_instructions.clone();

                debug!("Adding job for {:?}", address);

                threadpool.1.execute(move || {
                    debug!("Thread started for {:?}", address);

                    // Shuffle the instruction buffer by line
                    // Performace hungry (probably mostly because of the random shuffle), but only for under a second
                    let pixelflood_instructions: Vec<u8> = {
                        let mut lines: Vec<&[u8]> = pixelflood_instructions
                            .split(|char| char == &b"\n"[0])
                            .collect();
                        lines.shuffle(&mut thread_rng());
                        lines.join(&b"\n"[0])
                    };

                    // Create a new TcpStream from a "lower level" socket
                    // This is needed to bind the socket to a specific address
                    let mut stream = {
                        let socket = Socket::new(
                            if address.is_ipv4() {
                                Domain::ipv4()
                            } else {
                                Domain::ipv6()
                            },
                            Type::stream(),
                            Some(Protocol::tcp()),
                        )
                        .unwrap();

                        // Bind the socket to the specific address
                        // Idk why a port is needed, but I just set it to 0
                        socket.bind(&SocketAddr::new(address, 0u16).into()).unwrap();

                        // Try every ip the target host resolved to
                        let mut connected = false;
                        for address in target_address {
                            match socket.connect(&address.into()) {
                                Ok(_) => {
                                    connected = true;
                                    break;
                                }
                                Err(e) => {
                                    debug!("Connection to address {:?} failed: {:?}", address, e)
                                }
                            }
                        }
                        if !connected {
                            panic!("Connecting failed");
                        }

                        // Convert / Wrap the socket into a TcpStream, because it's easier to work with
                        socket.into_tcp_stream()
                    };

                    // Apply the offset before the rest of the instruction
                    // This is only needed once a socket is freshly connected
                    stream
                        .write_all(&format!("OFFSET {} {}\n", offset.0, offset.1).into_bytes())
                        .unwrap();

                    // Dump the buffer in a loop
                    // Possible performace improvements? (Ideas: custom write_all, manual flushing, endless buffers, ...)
                    loop {
                        stream.write_all(&pixelflood_instructions).unwrap();
                    }
                });
            }

            // Sleep a bit, so the thread manager doesn't need much cpu
            // This also really helps if the target_host is unreachable and the threads are constantly crashing
            thread::sleep(Duration::from_secs_f64(0.1));
        });
    }
}
